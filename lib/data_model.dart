class ModelData {
  String icon;
  String name;
  String desc;

  ModelData({this.icon, this.name, this.desc});

  ModelData.fromJson(dynamic json) {
    icon = json['icon'] as String;
    name = json['name'] as String;
    desc = json['desc'] as String;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['icon'] = icon;
    data['name'] = name;
    data['desc'] = desc;
    return data;
  }
}