import 'dart:convert';
import 'dart:core';

import 'package:demo_json/data_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    Provider.of<HomeProvider>(context, listen: false)
        .getData()
        .then((_) => {print('done')});
  }

  @override
  Widget build(BuildContext context) {
    final List<ModelData> listData = Provider.of<HomeProvider>(context, listen: true).datas;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: Container(
        child: ListView.builder(
            itemCount: listData.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                child: Row(
                  children: [
                    Container(
                      width: 120,
                      height: 120,
                      child: Image.network('${listData[index].icon}',width: 120,),

                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('${listData[index].name}'),
                          Text('${listData[index].desc}'),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            }),
      ),
    );
  }
}

class HomeProvider extends ChangeNotifier {
  List<ModelData> datas = [];

  Future<void> getData() async {
    try {
      final String data = await loadAsset();
      final jsonMode = jsonDecode(data) as List<dynamic>;
      datas = jsonMode.map((dynamic data) => ModelData.fromJson(data)).toList();
      print(datas);
      notifyListeners();
    } catch (e) {
      print('================$e');
    }
  }
}

Future<String> loadAsset() async {
  return await rootBundle.loadString('assets/res/data.txt');
}
